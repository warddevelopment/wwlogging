//
//  WWLoggingTests.m
//  WWLoggingTests
//
//  Created by Wills Ward on 2/16/14.
//  Copyright (c) 2014 WillWorks. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WWLogging.h"

@interface WWLoggingTests : XCTestCase

@end

@implementation WWLoggingTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
	WWLog(WWLogtypeMESSAGE, 3, @"Hi %@", @"bye");
}

@end
