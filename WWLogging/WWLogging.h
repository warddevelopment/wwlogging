//
//  WWLogging.h
//  WWLogging
//
//  Created by Wills Ward on 2/16/14.
//  Copyright (c) 2014 WillWorks. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
	WWLogtypeERROR,
	WWLogtypeWARNING,
	WWLogtypeMESSAGE
} WWLogType;


static void WWError(NSString* format, ...);
static void WWErrorWithCode(int code, NSString* format, ...);
static void WWWarning(NSString* format, ...);
static void WWWarningWithCode(int code, NSString* format, ...);
static void WWMessage(NSString* format, ...);
static void WWMessageWithCode(int code, NSString* format, ...);
static void WWLog(WWLogType, int code, NSString* format, ...);


static void WWError(NSString* format, ...)
{
	va_list args;
	va_start(args, format);
	NSString* string = [[NSString alloc] initWithFormat:format arguments:args];
	string = [@"<ERROR>:" stringByAppendingString:string];
	va_end(args);
	
	NSLog(@"%@", string, nil);

}

static void WWErrorWithCode(int code, NSString* format, ...)
{
	va_list args;
	va_start(args, format);
	NSString* string = [[NSString alloc] initWithFormat:format arguments:args];
	string = [NSString stringWithFormat:@"<ERROR: %d>: %@", code, string];
	va_end(args);
	
	NSLog(@"%@", string, nil);
	
}

static void WWWarning(NSString* format, ...)
{
	va_list args;
	va_start(args, format);
	NSString* string = [[NSString alloc] initWithFormat:format arguments:args];
	string = [@"<WARNING>:" stringByAppendingString:string];
	va_end(args);
	
	NSLog(@"%@", string, nil);
	
}

static void WWWarningWithCode(int code, NSString* format, ...)
{
	va_list args;
	va_start(args, format);
	NSString* string = [[NSString alloc] initWithFormat:format arguments:args];
	string = [NSString stringWithFormat:@"<WARNING: %d>: %@", code, string];
	va_end(args);
	
	NSLog(@"%@", string, nil);
}

static void WWMessage(NSString* format, ...)
{
	va_list args;
	va_start(args, format);
	NSString* string = [[NSString alloc] initWithFormat:format arguments:args];
	string = [@"<MESSAGE>:" stringByAppendingString:string];
	va_end(args);
	
	NSLog(@"%@", string, nil);
	
}

static void WWMessageWithCode(int code, NSString* format, ...)
{
	va_list args;
	va_start(args, format);
	NSString* string = [[NSString alloc] initWithFormat:format arguments:args];
	string = [NSString stringWithFormat:@"<MESSAGE: %d>: %@", code, string];
	va_end(args);
	
	NSLog(@"%@", string, nil);
}

static void WWLog(WWLogType type, int code, NSString* format, ...)
{
	va_list args;
	va_start(args, format);
	NSString* string = [[NSString alloc] initWithFormat:format arguments:args];
	va_end(args);
	
	if (type == WWLogtypeERROR) WWLogErrorWithCode(code, string);
	if (type == WWLogtypeWARNING) WWLogWarningWithCode(code, string);
	if (type == WWLogtypeMESSAGE) WWLogMessageWithCode(code, string);
}

@interface WWLogging : NSObject

@end
